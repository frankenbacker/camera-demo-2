package ru.rkn.customcameraapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by rkn on 07.03.18.
 */

public class GalleryItemAdapter extends RecyclerView.Adapter<GalleryItemAdapter.ViewHolder> implements View.OnClickListener {
    private Context context;
    private ArrayList<MediaFile> mediaFiles;
    private OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public GalleryItemAdapter(Context context, ArrayList<MediaFile> mediaFiles, OnItemClickListener listener) {
        this.context = context;
        this.mediaFiles = mediaFiles;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_gallery, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (mediaFiles.get(position).isVideo()) {
            holder.ext.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_gallery_item_play));
        } else {
            holder.ext.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_gallery_item_photo));
        }
        new AsyncLoadImages(holder.image).execute(mediaFiles.get(position));
        holder.bind(position, listener);
    }

    @Override
    public int getItemCount() {
        return mediaFiles.size();
    }

    @Override
    public void onClick(View view) {

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        ImageView ext;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.item_gallery_image);
            image.setScaleType(ImageView.ScaleType.CENTER_CROP);
            ext = itemView.findViewById(R.id.item_gallery_ext);

            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            int width = metrics.widthPixels;
            int height = metrics.heightPixels;

            image.setLayoutParams(new CardView.LayoutParams(width/2, width/2));
        }

        public void bind(final int position, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(position);
                }
            });
        }
    }

    private class AsyncLoadImages extends AsyncTask<MediaFile, Void, Bitmap> {
        ImageView imageView;

        AsyncLoadImages(ImageView imageView) {
            this.imageView = imageView;
        }

        @Override
        protected Bitmap doInBackground(MediaFile... files) {
            final MediaFile file = files[0];
            String url = file.getPath();

            if (file.isVideo()) {
                MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                mmr.setDataSource(url);

                Bitmap bitmap = mmr.getFrameAtTime(0, MediaMetadataRetriever.OPTION_CLOSEST);

                return bitmap;
            }
            else {
                // Get the dimensions of the View
                int targetW = 200;
                int targetH = 200;

                // Get the dimensions of the bitmap
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                bmOptions.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(url, bmOptions);
                int photoW = bmOptions.outWidth;
                int photoH = bmOptions.outHeight;

                // Determine how much to scale down the image
                int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

                // Decode the image file into a Bitmap sized to fill the View
                bmOptions.inJustDecodeBounds = false;
                bmOptions.inSampleSize = scaleFactor;
                bmOptions.inPurgeable = true;

                Bitmap bitmap = BitmapFactory.decodeFile(url, bmOptions);

                return bitmap;
            }
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            imageView.setImageBitmap(result);
        }
    }
}
