package ru.rkn.customcameraapp;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class EditorActivity extends AppCompatActivity {

    private ImageView image;
    private VideoView video;
    private ImageButton goback;
    private ImageButton crop;
    private ImageButton save;

    private boolean isPhoto = true;
    private boolean isLoaded = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_editor);
        image = findViewById(R.id.editor_image);
        video = findViewById(R.id.editor_video);
        goback = findViewById(R.id.editor_goback);
        crop = findViewById(R.id.editor_crop);
        save = findViewById(R.id.editor_save);

        crop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(EditorActivity.this, "Скоро появится", Toast.LENGTH_SHORT).show();
            }
        });

        if (getIntent().getIntExtra("data", 0) == 1) {
            // Фото
            isPhoto = true;
            video.setVisibility(View.GONE);
            image.setVisibility(View.VISIBLE);
            crop.setVisibility(View.VISIBLE);
            byte[] bytes = GlobalVars.capturedImage;
            Bitmap bitmap = null;
            if (bytes != null) {
                bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            } else if (GlobalVars.bitmap != null) {
                bitmap = GlobalVars.bitmap;
            }
            Matrix matrix = new Matrix();
            int orientation = 0;
            if (getIntent().getIntExtra("cam", 0) == 1) {
                orientation = 90;
            } else if (getIntent().getIntExtra("cam", 0) == 2) {
                orientation = 270;
            }
            matrix.postRotate(orientation);
            Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap , 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

            image.setImageBitmap(rotatedBitmap);
        } else {
            // Видео
            isPhoto = false;
            crop.setVisibility(View.GONE);
            image.setVisibility(View.GONE);
            video.setVisibility(View.VISIBLE);

            if (getIntent().getStringExtra("path") != null) {
                String path = getIntent().getStringExtra("path");
                video.setVideoPath(path);
                Log.d("videoPath", path);
            } else {
                File file = new File(getFilesDir(), "cached.mp4");
                video.setVideoPath(file.getAbsolutePath());
            }
            //video.setMediaController(new MediaController(this));
            video.requestFocus(0);
            video.setZOrderOnTop(false);
            video.start();
            isLoaded = true;
            video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    video.start();
                }
            });
        }

        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isOk = true;
                if (isPhoto) {
                    byte[] bytes = GlobalVars.capturedImage;
                    Bitmap bitmap = GlobalVars.bitmap;
                    if (bytes != null) {
                        bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                    }
                    if (bitmap != null) {
                        String fileName = System.currentTimeMillis() + ".jpg";
                        File file = new File(getFilesDir() + "/images", fileName);

                        Matrix matrix = new Matrix();
                        int orientation = 0;
                        if (getIntent().getIntExtra("cam", 0) == 1) {
                            orientation = 90;
                        } else if (getIntent().getIntExtra("cam", 0) == 2) {
                            orientation = 270;
                        }
                        matrix.postRotate(orientation);
                        Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap , 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

                        try {
                            FileOutputStream fos = new FileOutputStream(file);
                            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                            fos.flush();
                            fos.close();
                            Log.d("file", file.getAbsolutePath());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        isOk = false;
                    }
                } else {
                    String path = getIntent().getStringExtra("path");
                    if (path != null) {
                        File file = new File(path);
                        File out = new File(getFilesDir(), "cached.mp4");
                        try {
                            FileChannel src = new FileInputStream(file).getChannel();
                            FileChannel dst = new FileOutputStream(out).getChannel();
                            dst.transferFrom(src, 0, src.size());
                            src.close();
                            dst.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    File file = new File(getFilesDir(), "cached.mp4");
                    if (file.exists()) {
                        isOk = file.renameTo(new File(getFilesDir() + "/images", System.currentTimeMillis() + ".mp4"));
                    } else {
                        isOk = false;
                    }
                }
                if (isOk) {
                    Toast.makeText(getApplicationContext(), "Сохранено", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(EditorActivity.this, GalleryActivity.class));
                    finishAffinity();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isLoaded) {
            video.start();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (deleteCached()) {
            // Файл удален
            Log.d("CACHED", "file deleted");
        }
    }

    // При сохранении видео в галерее, назад
    private boolean deleteCached() {
        File file = new File(getFilesDir(), "cached.mp4");
        GlobalVars.capturedImage = null;
        return file.delete();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }
}
