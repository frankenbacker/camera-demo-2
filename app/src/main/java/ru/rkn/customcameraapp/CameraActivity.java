package ru.rkn.customcameraapp;

import android.app.Instrumentation;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.CountDownTimer;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class CameraActivity extends AppCompatActivity implements SurfaceHolder.Callback {
    private static final int MEDIA_TYPE_IMAGE = 170;
    private static final int MEDIA_TYPE_VIDEO = 796;

    private static final int CAMERA_BACK = 468;
    private static final int CAMERA_FRONT = 702;

    //private boolean isSnapTaken = false;

    private Camera camera;
    private MediaRecorder mediaRecorder;

    private int whichCam = CAMERA_BACK;
    private boolean isFlashEnabled = false;

    private SurfaceView camView;
    private TextView duration;
    private ImageButton snap;
    private ImageButton goback;
    private ImageButton gallery;
    private ImageButton flash;
    private ImageButton swap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_camera);

        final Drawable flashOn = ContextCompat.getDrawable(this, R.drawable.ic_flash_on);
        final Drawable flashOff = ContextCompat.getDrawable(this, R.drawable.ic_flash_off);
        final Drawable camBack = ContextCompat.getDrawable(this, R.drawable.ic_camera_back);
        final Drawable camFront = ContextCompat.getDrawable(this, R.drawable.ic_camera_front);

        duration = findViewById(R.id.camera_duration);

        camView = findViewById(R.id.camera_view);
        camView.getHolder().addCallback(this);

        snap = findViewById(R.id.camera_snap);
        snap.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                // initialize video camera
                if (prepareVideoRecorder()) {
                    // Camera is available and unlocked, MediaRecorder is prepared,
                    // now you can start recording
                    mediaRecorder.start();
                    recordVideo.start();

                    Animation anim = AnimationUtils.loadAnimation(CameraActivity.this, android.R.anim.fade_in);
                    duration.startAnimation(anim);
                    duration.setVisibility(View.VISIBLE);
                } else {
                    // prepare didn't work, release the camera
                    releaseMediaRecorder();
                    return false;
                }
                // Костыль???
                snap.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                            recordVideo.onFinish();
                            recordVideo.cancel();
                        }
                        return false;
                    }
                });
                return true;
            }
        });
        snap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                camera.takePicture(null, null, new Camera.PictureCallback() {
                    @Override
                    public void onPictureTaken(byte[] bytes, Camera cam) {
                        if (bytes != null) {
//                            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
//                            if (bitmap != null) {
//                                String fileName = System.currentTimeMillis() + ".jpg";
//                                File file = new File(getFilesDir() + "/images", fileName);
//                                try {
//                                    FileOutputStream fos = new FileOutputStream(file);
//                                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
//                                    fos.flush();
//                                    fos.close();
//                                    Log.d("file", file.getAbsolutePath());
//                                } catch (IOException e) {
//                                    e.printStackTrace();
//                                }
//                            }
                            releaseCamera();
                            GlobalVars.capturedImage = bytes;
                            Intent i = new Intent(CameraActivity.this, EditorActivity.class);
                            i.putExtra("data", 1);
                            if (whichCam == CAMERA_BACK) {
                                i.putExtra("cam", 1);
                            } else {
                                i.putExtra("cam", 2);
                            }
                            startActivity(i);
                        } else {
                            // Не удалось сделать фото
                        }
                    }
                });
                Log.d("CAM", "takePhoto");
                camera.startPreview();
            }
        });

        goback = findViewById(R.id.camera_goback);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        gallery = findViewById(R.id.camera_pick_gallery);
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showChooser();
            }
        });

        flash = findViewById(R.id.camera_flash);
        flash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (whichCam == CAMERA_BACK) {
                    Camera.Parameters params = camera.getParameters();
                    Drawable flashIco;
                    if (!isFlashEnabled) {
                        isFlashEnabled = true;
                        params.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                        flashIco = flashOn;
                    } else {
                        isFlashEnabled = false;
                        params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                        flashIco = flashOff;
                    }
                    camera.setParameters(params);
                    flash.setImageDrawable(flashIco);
                }
            }
        });

        swap = findViewById(R.id.camera_swap);
        swap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                camera.stopPreview();
                camera.release();

                if (Camera.getNumberOfCameras() > 1) {
                    if (whichCam == CAMERA_BACK) {
                        whichCam = CAMERA_FRONT;
                        camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
                    } else if (whichCam == CAMERA_FRONT) {
                        whichCam = CAMERA_BACK;
                        camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
                    } else {
                        camera = Camera.open();
                    }
                    camera.setDisplayOrientation(90);
                }

                if (whichCam == CAMERA_FRONT) {
                    isFlashEnabled = false;
                    Camera.Parameters params = camera.getParameters();
                    params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    camera.setParameters(params);
                    flash.setImageDrawable(flashOff);
                }

                List<String> focusModes = camera.getParameters().getSupportedFocusModes();
                if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
                    Camera.Parameters params = camera.getParameters();
                    params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                    camera.setParameters(params);
                }

                try {
                    camera.setPreviewDisplay(camView.getHolder());
                    camera.startPreview();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Camera.Parameters parameters = camera.getParameters();
                parameters.setPictureSize(parameters.getPreviewSize().width, parameters.getPreviewSize().height);
                camera.setParameters(parameters);

                swap.setImageDrawable((whichCam != CAMERA_BACK ? camFront : camBack));
            }
        });
    }

    private void showChooser() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/* video/*");
        startActivityForResult(Intent.createChooser(intent, "Выберите медиафайл"), 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            final Uri uri = data.getData();
            if (uri.toString().contains("image")) {
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                    GlobalVars.bitmap = bitmap;
                    Intent i = new Intent(CameraActivity.this, EditorActivity.class);
                    i.putExtra("data", 1);
                    startActivity(i);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else  if (uri.toString().contains("video")) {
                //handle video
                String path = getPath(uri);
                if (path != null) {
                    Intent i = new Intent(CameraActivity.this, EditorActivity.class);
                    i.putExtra("path", path);
                    startActivity(i);
                }
            }
        }
    }

    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Video.Media.DATA };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }

    private CountDownTimer recordVideo = new CountDownTimer(31000, 1000) {
        int i = 0;

        @Override
        public void onTick(long l) {
            Log.d("REC", String.valueOf(i));
            if (i < 10) {
                duration.setText("0:0" + i + "/0:30");
            } else {
                duration.setText("0:" + i + "/0:30");
            }
            i++;
        }

        @Override
        public void onFinish() {
            i = 0;
            // stop recording and release camera
            mediaRecorder.stop();  // stop the recording
            releaseMediaRecorder(); // release the MediaRecorder object
            camera.lock();         // take camera access back from MediaRecorder
            Log.d("REC", "stopped");

            snap.setOnTouchListener(null);

            Animation anim = AnimationUtils.loadAnimation(CameraActivity.this, android.R.anim.fade_out);
            duration.startAnimation(anim);
            duration.setVisibility(View.GONE);

            startActivity(new Intent(CameraActivity.this, EditorActivity.class));
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        if (Camera.getNumberOfCameras() > 1) {
            if (whichCam == CAMERA_BACK) {
                camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
            } else if (whichCam == CAMERA_FRONT) {
                camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
            } else {
                camera = Camera.open();
            }
        }
        camView.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        releaseMediaRecorder();
        releaseCamera();
        camView.setVisibility(View.GONE);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    private Camera.Size getOptimalSize(Camera.Parameters params, int w, int h) {
        final double ASPECT_TH = .2; // Threshold between camera supported ratio and screen ratio.
        double minDiff = Double.MAX_VALUE; //  Threshold of difference between screen height and camera supported height.
        double targetRatio = 0;
        int targetHeight = h;
        double ratio;
        Camera.Size optimalSize = null;

        // check if the orientation is portrait or landscape to change aspect ratio.
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            targetRatio = (double) h / (double) w;
        } else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            targetRatio = (double) w / (double) h;
        }

        // loop through all supported preview sizes to set best display ratio.
        for (Camera.Size s : params.getSupportedPreviewSizes()) {

            ratio = (double) s.width / (double) s.height;
            if (Math.abs(ratio - targetRatio) <= ASPECT_TH) {

                if (Math.abs(targetHeight - s.height) < minDiff) {
                    optimalSize = s;
                    minDiff = Math.abs(targetHeight - s.height);
                }
            }
        }
        return optimalSize;
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        try {
            camera.setPreviewDisplay(surfaceHolder);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Camera.Parameters parameters = camera.getParameters();
        parameters.setPictureSize(parameters.getPreviewSize().width, parameters.getPreviewSize().height);
        camera.setParameters(parameters);

        Camera.Size previewSize = camera.getParameters().getPreviewSize();

        float aspect = (float) previewSize.width / previewSize.height;

        int previewSurfaceWidth = camView.getWidth();
        int previewSurfaceHeight = camView.getHeight();

        ViewGroup.LayoutParams lp = camView.getLayoutParams();

        if (this.getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
            camera.setDisplayOrientation(90);
            lp.height = previewSurfaceHeight;
            lp.width = (int) (previewSurfaceHeight / aspect);
        } else {
            camera.setDisplayOrientation(0);
            lp.width = previewSurfaceWidth;
            lp.height = (int) (previewSurfaceWidth / aspect);
        }

        Camera.Parameters params = camera.getParameters();
        List<String> focusModes = params.getSupportedFocusModes();
        if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
            // Autofocus mode is supported
            params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            camera.setParameters(params);
        }

        camView.setLayoutParams(lp);
        camera.startPreview();
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

    }

    private boolean prepareVideoRecorder() {
        camera.unlock();

        mediaRecorder = new MediaRecorder();
        mediaRecorder.setCamera(camera);
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_480P));
//        Camera.Size optimalSize = getOptimalSize(camera.getParameters(),
//                camView.getLayoutParams().width,
//                camView.getLayoutParams().height);
//        mediaRecorder.setVideoSize(optimalSize.width, optimalSize.height);
        mediaRecorder.setMaxDuration(30000);
        if (whichCam == CAMERA_BACK) {
            mediaRecorder.setOrientationHint(90);
        } else if (whichCam == CAMERA_FRONT) {
            mediaRecorder.setOrientationHint(270);
        }
        //String fileName = System.currentTimeMillis() + ".mp4";
        File file = new File(getFilesDir(), "cached.mp4");
        mediaRecorder.setOutputFile(file.getAbsolutePath());
        mediaRecorder.setPreviewDisplay(camView.getHolder().getSurface());

        try {
            mediaRecorder.prepare();
        } catch (Exception e) {
            e.printStackTrace();
            releaseMediaRecorder();
            return false;
        }
        return true;
    }

    private void releaseMediaRecorder() {
        if (mediaRecorder != null) {
            mediaRecorder.reset();
            mediaRecorder.release();
            mediaRecorder = null;
            camera.lock();
        }
    }

    private void releaseCamera() {
        if (camera != null) {
            camera.setPreviewCallback(null);
            camera.stopPreview();
            camera.release();
            camera = null;
        }
    }
}
