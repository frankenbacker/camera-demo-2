package ru.rkn.customcameraapp;

import android.support.annotation.NonNull;
import android.util.Log;

import java.io.File;

/**
 * Created by rkn on 07.03.18.
 */

public class MediaFile {
    private String path;
    private String ext;
    private String name;

    public MediaFile() {}

    public MediaFile(@NonNull String path) {
        this.path = path;
        File file = new File(path);
        this.name = file.getName();
        this.ext = path.substring(path.lastIndexOf(".") + 1);
    }

    public String getPath() {
        return path;
    }

    public String getType() {
        return ext;
    }

    public String getName() {
        return name;
    }

    public boolean isVideo() {
        if (ext.contains("jpg")) {
            return false;
        }
        return true;
    }
}
