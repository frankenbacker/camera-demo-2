package ru.rkn.customcameraapp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

public class GalleryActivity extends AppCompatActivity implements GalleryItemAdapter.OnItemClickListener {
    private static final int ALLOW_PERMISSIONS = 559;
    private static final int OPEN_FILE = 309;

    private FloatingActionButton fab;
    private RecyclerView rv;
    private TextView noitems;

    private ArrayList<MediaFile> mediaFiles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        fab = findViewById(R.id.gallery_fab);
        rv = findViewById(R.id.gallery_rv);
        noitems = findViewById(R.id.gallery_noitems);

        noitems.setVisibility(View.VISIBLE);
        rv.setVisibility(View.GONE);

        // Поиск медиафайлов
        boolean isDirExist = false;

        File mediaStorageDir = new File(getFilesDir(), "images");
        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                Log.d(getString(R.string.app_name), "Не удалось создать директорию");
            } else {
                isDirExist = true;
            }
        } else {
            isDirExist = true;
        }

        mediaFiles = new ArrayList<>();
        if (isDirExist) {
            File[] files = mediaStorageDir.listFiles();
            for (File file : files) {
                MediaFile mediaFile = new MediaFile(file.getAbsolutePath());
                mediaFiles.add(0, mediaFile);
            }
        }

        Log.d("files", String.valueOf(mediaFiles.size()));
        if (mediaFiles.size() > 0) {
            rv.setHasFixedSize(true);
            rv.setItemViewCacheSize(50);
            rv.setDrawingCacheEnabled(true);
            rv.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_AUTO);

            rv.setLayoutManager(new GridLayoutManager(this, 2, LinearLayoutManager.VERTICAL, false));
            rv.addItemDecoration(new GridSpacingItemDecoration(2,
                    getResources().getDimensionPixelSize(R.dimen.grid_spacing), true));
            rv.setAdapter(new GalleryItemAdapter(this, mediaFiles, this));

            noitems.setVisibility(View.GONE);
            rv.setVisibility(View.VISIBLE);
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkPermissions()) {
                    startActivity(new Intent(GalleryActivity.this, CameraActivity.class));
                } else {
                    requestPermissions();
                }
            }
        });
    }

    private boolean checkPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//            return false;
//        }
        return true;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, new String[] {
                Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.READ_EXTERNAL_STORAGE,
//                Manifest.permission.WRITE_EXTERNAL_STORAGE
        }, ALLOW_PERMISSIONS);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        boolean isOk = true;
        if (requestCode == ALLOW_PERMISSIONS) {
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                    isOk = false;
                }
            }
            if (isOk) {
                startActivity(new Intent(this, CameraActivity.class));
            } else {
                // Сообщить пользователю разрешить доступ к камере
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onItemClick(int position) {
        File file = new File(mediaFiles.get(position).getPath());
        Uri uri = Uri.parse("content://" + BuildConfig.APPLICATION_ID + ".fileprovider/images/"
                + mediaFiles.get(position).getName());
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        if (mediaFiles.get(position).isVideo()) {
            intent.setDataAndType(uri,"video/*");
        } else {
            intent.setDataAndType(uri, "image/*");
        }
        startActivity(Intent.createChooser(intent, "Выберите приложение"));
    }
}
