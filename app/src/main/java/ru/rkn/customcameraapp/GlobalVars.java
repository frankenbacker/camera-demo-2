package ru.rkn.customcameraapp;

import android.graphics.Bitmap;

/**
 * Created by rkn on 10.03.18.
 */

public class GlobalVars {
    public static byte[] capturedImage = null;
    public static Bitmap bitmap = null;
}